#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
import cPickle
from cStringIO import StringIO
import inspect
import linecache
import optparse
import os
import sys

import sys
sys.path.append('build/lib.macosx-10.8-intel-2.7/')

from _custom_tracker import CustomTracker as CCustomTracker

def frame_pos_str(frame):
    return frame, frame.f_code.co_filename + \
        " >> " + frame.f_code.co_name + \
        "#" + str(frame.f_lineno) + \
        ' [' + ' ; '.join([i.strip() for i in inspect.getouterframes(frame)[0][4]]) + ']'

def handler(ct, frame, what):
	if ct.current_frame == frame:
	    print frame_pos_str(frame), what

class CustomTracker(CCustomTracker):
    def __enter__(self):
    	self.current_frame = inspect.currentframe().f_back
        super(CustomTracker, self).__enter__()


ct = CustomTracker(handler)

def fn1():
	print 'fn1'
	for i in range(0, 3):
		fn2(i)

def fn2(i):
	print 'fn2', i
	
with ct:
	print 'test'
	fn1()
	for i in range(5, 8):
		fn2(i)
"""